import numpy as np
from operator import lt,gt

a = (1,1)
b = (1,0)
c = (1,0)
d = (1,0)

seg1 = (a,b)
seg2 = (c,d)

def oriention(s1,s2,t):
    # x1(y2-y3)+x2(y3-y1)+x3(y1-y2)
    # x - [0]
    # y - [1]
    # 1 - s1, 2 - s2, 3 - t
    temp = s1[0]*(s2[1]-t[1]) + s2[0]*(t[1]-s1[1]) + t[0]*(s1[1]-s2[1])
    return np.sign(temp)

def segment_length(a,b):
    return np.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

#returns point with min/max x/y coordinates
# min_max = operator lt for min, operator gt for max
#x_y = 0 for x, 1 for y
def minmax_xy(seg1,seg2,min_max,x_y):

    min_x=seg1[0][x_y]
    min_p=seg1[0]

    if(min_max(seg1[1][x_y],min_x)):
        min_x=seg1[1][x_y]
        min_p=seg1[1]

    if(min_max(seg2[0][x_y],min_x)):
        min_x=seg2[0][x_y]
        min_p=seg2[0]

    if(min_max(seg2[1][x_y],min_x)):
        min_x=seg2[1][x_y]
        min_p=seg2[1]

    return min_p

def segment_intersect(seg1,seg2):
    seg1_seg2tail = oriention(seg1[0],seg1[1],seg2[0])
    seg1_seg2head = oriention(seg1[0],seg1[1],seg2[1])

    seg2_seg1tail = oriention(seg2[0],seg2[1],seg1[0])
    seg2_seg1head = oriention(seg2[0], seg2[1], seg1[1])

    """
    4 mogucnosti
    
    i) Sve orijentacije razlicite od nule, uslov zadovoljen sijeku se u suprotnom ne
    
    ii) Tacno dvije orijentacije su nula - segmenti imaju jednu zajednicku tacku i to 
        head/tail segmenta1 se poklapa sa head/tail segmenta2
        0!=+/-1 and 0!=+/-1
        Prvi uslov je zadovoljen pa se sijeku
    
    iii) Tacno jedna orijentacija je 0 - 3 tacke duzi su kolinearne
        Bez smanjenja opstosti pretpostavimo da je seg1_seg2tail=0. Tada je njegov par 
        seg1_seg2head = +/-1 (nevazno).
        Tada presjek duzi zavisi od orijentacije drugog para.
        Ako je seg2_seg1tail = seg2_seg1head duzi se ne sijeku. U suprotnom se sijeku
        0!=+/-1 and 1!=-1
        Uslov zadovoljen
    
    iv) Sve orijentacije su nule - sve tacke su kolinearne
        Ako je zbir duzina 2 segmenta AB+CD veci od duzine segmenta koji cine tacka sa
        najmanjom x-koordinatom i najvecom x-koordinatom, segmenti se sijeku. U suprotno ne
        (ako su sve x koordinate jednake tada trazimo po y )
            
    """
    if(seg1_seg2tail!=seg1_seg2head and seg2_seg1tail!=seg2_seg1head):
        return True
    elif(seg1_seg2tail==seg1_seg2head==seg2_seg1tail==seg2_seg1head==0):

        ab_cd=segment_length(seg1[0],seg1[1])+segment_length(seg2[0],seg2[1])

        if(not(seg1[0][0]==seg1[1][0]==seg2[0][0]==seg2[1][0])):
            min_x_p = minmax_xy(seg1,seg2,lt,0)
            max_x_p = minmax_xy(seg1,seg2,gt,0)
            max_length = segment_length(min_x_p,max_x_p)

            if(ab_cd >= max_length):
                return True
            else:
                return False
        elif(not(seg1[0][1]==seg1[1][1]==seg2[0][1]==seg2[1][1])):
            min_y_p = minmax_xy(seg1,seg2,lt,1)
            max_y_p = minmax_xy(seg1, seg2, gt, 1)
            max_length = segment_length(min_y_p, max_y_p)

            if (ab_cd >= max_length):
                return True
            else:
                return False
        else:#sve tacke se poklapaju
            return True
    else:
        return False

print(segment_intersect(seg1,seg2))
