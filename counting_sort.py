import numpy as np

nums = []
temp = input("Enter a number")

while temp.isnumeric():
    nums.append(int(temp))
    temp = input("Enter a number")

min = min(nums)
max = max(nums)

num_range = np.arange(min,max+1)
num_count = np.zeros(num_range.size,dtype=int)

for i in range(num_count.size):
    num_count[i] = nums.count(num_range[i])

sum = 0
for i in range(num_count.size):
    sum+=num_count[i]
    num_count[i] = sum

for i in range(num_count.size-1 , 0,-1):
    num_count[i] = num_count[i-1]

num_count[0] = 0

sorted_list = np.zeros(len(nums),dtype=int)
num_dict = dict(zip(num_range,num_count))

for i in range(len(nums)):
    temp_num = nums[i]
    index = num_dict[temp_num]
    sorted_list[index] = temp_num
    num_dict[temp_num]+=1

print(sorted_list)