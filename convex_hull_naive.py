import numpy as np

class Vertex:
    def __init__(self,x=None,y=None,vertex=None):
        if vertex:
            self.x=vertex.x
            self.y=vertex.y
        else:
            self.x=x
            self.y=y

    def __eq__(self, other):
        if isinstance(other, Vertex):
            return self.x==other.x and self.y==other.y
        return NotImplemented

    def __ne__(self,other):
        return not self.__eq__(other)

    def __str__(self):
        return "x="+str(self.x)+" y="+str(self.y)


class Edge:
    def __init__(self,v1,v2):
        self.v1=v1
        self.v2=v2

    def __eq__(self, other):
        if isinstance(other, Edge):
            return self.v1==other.v1 and self.v2==other.v2

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return "head: "+str(self.v1)+" tail: "+str(self.v2)

x1=Vertex(0, 0)
x2=Vertex(1, 1)
x3=Vertex(2, 0)
x4=Vertex(0, 2)
x5=Vertex(2, 2)
x6=Vertex(1, 0)
x7=Vertex(3, 0)


vertices = (x1,x2,x3,x4,x5,x6,x7)
edges=[]

for p1 in vertices:
    for p2 in vertices:
        if p1!=p2:
            edges.append(Edge(p1, p2))
"""
Ideja je prvo napraviti omotac tako sto cemo funkcijom all_points_right provjeravati za svaku duz
da li se sve tacke nalaze sa njene desne strane
Nakon toga potrebno je ivice omotaca sortirati 
"""


def naive_convex_hull(vertices,edges):
    hull=[]
    for e in edges:
        all_right = all_points_right(e,vertices)
        if all_right:
            hull.append(e)

    sorted_hull = sort_hull(hull)
    return sorted_hull

def all_points_right(e,vertices):
    for v in vertices:
        if ori(e,v) > 0:
            return False
    return True

def ori(e, v):
    # x1(y2-y3)+x2(y3-y1)+x3(y1-y2)
    temp = e.v1.x * (e.v2.y - v.y) + e.v2.x * (v.y - e.v1.y) + v.x* (e.v1.y - e.v2.y)
    return np.sign(temp)

def sort_hull(hull):

    # Uzimamo neku pocetnu ivicu iz hull, ako ona nije kolinearna ni sa jednom drugom ivicom iz hull
    # onda pocinjemo sa njom, ako jeste kolinearna sa nekim ivicama onda uzimamo najduzu ivicu iz tog skupa
    e_temp = hull[-1]
    #vraca niz ivica koje su kolinearne sa e_temp, ako takvih nema vraca samo niz sa e_temp
    collinear_edges = collinear(e_temp,hull)
    if len(collinear_edges)==1:
        sorted_hull = collinear_edges
        hull.remove(collinear_edges[0])
    else:
        sorted_hull=[]
        sorted_hull.append(max(collinear_edges,key=lambda e: modulo(e.v1,e.v2)))
        for e in collinear_edges:
            hull.remove(e)

    while hull:
        next_edge = find_next_edge(sorted_hull[-1],hull)
        if next_edge==None:#edge case, kada imamo kolinearne ivice, vidjeti find_next_edge
            break
        sorted_hull.append(next_edge)

    return sorted_hull

def collinear(e,hull):
    collinear_edges = []
    for edge in hull:
        if ori(e,edge.v1)==0 and ori(e,edge.v2)==0 and ori(edge,e.v1)==0 and ori(edge,e.v2)==0:
            collinear_edges.append(edge)

    return collinear_edges

def find_next_edge(e,hull):
    target_edges = []
    for edge in hull:
        if edge.v2==e.v1:
            target_edges.append(edge)

    if len(target_edges)==1:
        target_edge=target_edges.pop()
        hull.remove(target_edge)
        return target_edge
    elif len(target_edges)>0:
        #ako se glava posljednjeg vektora u sorted hull poklapa sa repom vise vektora onda
        #su ti vektori kolinearni pa ubacujemo samo vektor sa najvecim intenzitetom a ostale izbacujemo
        #|
        #|
        #|
        #v-->--->--->
        #----------->
        #preostali vektori koje ne obuhvatamo a koje smo "prosli" i koji ostaju u hull
        #    --->
        #    ------->
        #        --->
        #ne kvare algoritam jer tada ova funkcija vraca None i taj slucaj je uzet u obzir
        max_e=max(target_edges,key=lambda e: modulo(e.v1,e.v2))

        target_edges.remove(max_e)
        hull.remove(max_e)
        for e in target_edges:
            hull.remove(e)
        return max_e

def modulo(v1,v2):
    return np.sqrt(np.power(v1.x-v2.x,2)+np.power(v1.y-v2.y,2))

convex_hull = naive_convex_hull(vertices,edges)

for e in convex_hull:
    print(e)
