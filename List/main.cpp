#include <iostream>

using namespace std;

class Cvor {
    protected:
        Cvor* naredniC;
        Cvor* prethodniC;
    public:
        Cvor();
        virtual ~Cvor(){};
        Cvor* sljedeci();
        Cvor* prethodni();
        Cvor* ubaci(Cvor*);
        Cvor* ukloni();
        void spoji(Cvor *);
};

Cvor::Cvor(): prethodniC(this), naredniC(this){};
Cvor* Cvor::sljedeci(){return naredniC;}
Cvor* Cvor::prethodni(){return prethodniC;}
Cvor* Cvor::ubaci(Cvor* b){
    Cvor* c = naredniC;
    b->naredniC=c;
    b->prethodniC= this;
    naredniC = b;
    c->prethodniC = b;
    return b;
}
Cvor* Cvor::ukloni(){
    naredniC->prethodniC = prethodniC;
    prethodniC->naredniC = naredniC;
    naredniC=prethodniC=this;
    return this;
}
void Cvor::spoji(Cvor* b){
        Cvor* c = this;
        Cvor* temp1 = c->naredniC;
        Cvor* temp2 = b->naredniC;
        temp1->prethodniC = b;
        b->naredniC = temp1;
        c->naredniC = temp2;
        temp2->prethodniC = c;
}

template<class T>
class Lista;

template<class T>
class ListaCvorova: public Cvor{
    public:
        T elementG;
        ListaCvorova(T element);
        ListaCvorova();
        friend class Lista<T>;
};

template<class T>
ListaCvorova<T>::ListaCvorova(T element):elementG(element){}

template<class T>
ListaCvorova<T>::ListaCvorova(){};

template<class T>
class Lista{
    private:
        ListaCvorova<T> *glavaL;
        ListaCvorova<T> *trenutniC;
        int duzinaL;
    public:
        Lista();
        T prviE();
        T ukloniE();
        ~Lista();
        T posljednjiE();
        T ubaciE(T);
        T prethodniE();
        T sljedeciE();
        bool jelPrviE();
        bool jelPosljednjiE();
        int vratiDuzinuL();
        bool jelGlavaL();
        T dodajNaPocetakE(T);
        T dodajNaKrajE(T);
        void postaviE(T);
        T vratiE();
        Lista *spojiL(Lista*);
};

template<class T>
Lista<T>::Lista():duzinaL(0){
    glavaL = new ListaCvorova<T>(0)//?
    trenutniC = glavaL;
}

template<class T>
T Lista<T> :: prviE(){
    trenutniC = (ListaCvorova<T>*)glavaL->prethodniC;
    return trenutniC->elementG;
}

template<class T>
T Lista<T>::ukloniE(){
    if (trenutniC == glavaL) return 0;
    void *el = trenutniC->elementG;
    trenutniC = (ListaCvorova<T>*)trenutniC->prethodni();
    delete (ListaCvorova<T>* )trenutniC->sljedeci()->ukloni();
    --duzinaL;
    return el;
}

template<class T>
Lista<T> :: ~Lista(){
    while (duzinaL > 0){
        prviE();
        ukloniE();
    }
    delete glavaL;
}

template<class T>
T Lista<T> :: posljednjiE(){
    trenutniC = (ListaCvorova<T>*)glavaL->sljedeci();
    return trenutniC->elementG;
}

template<class T>
T Lista<T> :: ubaciE(T el){
    trenutniC->ubaci(new ListaCvorova<T>(el));
    ++duzinaL;
    return el;
}

template<class T>
T Lista<T> :: prethodniE(){
 trenutniC = (ListaCvorova<T>*)trenutniC->prethodni();
 return trenutniC->elementG;
}

template<class T>
T Lista<T> :: sljedeciE(){
    trenutniC= (ListaCvorova<T>*)trenutniC->sljedeci();
    return trenutniC->elementG;
}

template<class T>
bool Lista<T>::jelPrviE(){
    return ((trenutniC == glavaL->prethodni()) && (duzinaL > 0));
}

template<class T>
bool Lista<T> :: jelPosljednjiE(){
    return ((trenutniC == glavaL->sljedeci()) && (duzinaL > 0));
}

template<class T>
int Lista<T> :: vratiDuzinuL(){
    return duzinaL;
}

template<class T>
bool Lista<T> :: jelGlavaL(){
    return trenutniC == glavaL;
}

template<class T>
T Lista<T> :: dodajNaPocetakE(T el){
    glavaL->prethodni()->ubaci(new ListaCvorova<T>(el));
    ++duzinaL;
    return el;
}

template<class T>
T Lista<T> :: dodajNaKrajE(T el){
    glavaL->ubaci(new ListaCvorova<T>(el));
    ++duzinaL;
    return el;
}

template<class T>
void Lista<T> :: postaviE(T el){
    if(trenutniC!=glavaL)trenutniC->elementG=el;
}

template<class T>
T Lista<T> :: vratiE(){
    return trenutniC->elementG;
}

template<class T>
Lista<T> * Lista<T> :: spojiL(Lista<T> *l){
    ListaCvorova<T> *a = (ListaCvorova<T>*)glavaL->prethodni();
    a->spoji(l->glavaL);
    duzinaL+=l->duzinaL;
    l->glavaL->ukloni();
    l->duzinaL= 0;
    l->trenutniC=glavaL;
    return this;
}

class Tacka
{
    private:
        double x,y;
    public:
        Tacka(){};
        Tacka(double, double);
        double dajX() const{return x;}
        double dajY() const{return y;}
};
Tacka::Tacka(double x, double y)
{
    this->x=x;
    this->y=y;
}




int main()
{
    Cvor* c = new Cvor();
    ListaCvorova<int>* listaC = new ListaCvorova<int>(3);
    cout<< listaC->elementG <<endl;

    Lista<int>* lista = new Lista<int>()

    Lista<Tacka*>* lista = new Lista<Tacka*>();
    Tacka ** niz= new Tacka*[5];
    for(int i=0;i<5;i++) niz[i]=lista->ubaciE(new Tacka(i,i));

    while(!lista->jelPosljednjiE()){
        Tacka * el=lista->sljedeciE();
        cout<<el->dajX()<<" "<<el->dajY()<<endl;
    }

    for(int i=0;i<8;i++){
        delete [] niz[i];
        niz[i]=nullptr;
    }
    delete [] niz;
    niz= nullptr;;
    delete lista;



    return 0;
}
