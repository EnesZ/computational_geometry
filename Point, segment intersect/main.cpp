#include <iostream>

using namespace std;

class Tacka{
 private:
    double x,y;
    bool izmedju(const Tacka &, const Tacka &);
 public:
     Tacka(double x = 0.0, double y = 0.0);
     Tacka(const Tacka &); //konstruktor kopije
     Tacka operator+(Tacka &);
     Tacka operator-(const Tacka &) const;
     friend Tacka operator*(double, Tacka &);
     double operator[](int) const;
     int operator==(const Tacka &) ;
     int operator!=(Tacka &);
     int operator<(Tacka &);
     int operator>(Tacka &);
     Tacka & operator =(const Tacka & p);
     double moduoV();
     int orjentacijaT(const Tacka &, const Tacka &);
     int klasifikacijaT(const Tacka &, const Tacka &);
     int klasifikacijaTM(const Tacka &, const Tacka &);
     double polarniUgao();
     double udaljenostS(Segment &);
};

Tacka::Tacka(double x1, double x2):x(x1),y(x2){}

Tacka::Tacka(const Tacka & kopija){
    this->x = kopija.x;
    this->y = kopija.y;
}

Tacka & Tacka :: operator =(const Tacka & p){
    this->x=p.x;
    this->y=p.y;
    return *this;
}

Tacka Tacka :: operator+(Tacka & p){
 return Tacka(x + p.x, y + p.y);
}

Tacka Tacka :: operator-(const Tacka & p) const{
 return Tacka(x - p.x, y - p.y);
}

Tacka operator*(double s, Tacka & p){
 return Tacka(s * p.x, s * p.y);
}

double Tacka :: operator[](int i) const{
     return (i == 0) ? x : y;
}

int Tacka::operator==(const Tacka &p){
     return (x == p.x) && (y == p.y);
}

int Tacka::operator!=(Tacka & p){
    return !(*this==p);
}

int Tacka::operator<(Tacka & p){
 return ((x < p.x) || ((x == p.x) && (y < p.y)));
}

int Tacka :: operator>(Tacka & p){
 return ((x > p.x) || ((x == p.x) && (y > p.y)));
}

int Tacka :: orjentacijaT(const Tacka & p1, const Tacka & p2){
    Tacka p3 = *this;
    Tacka vA = p2 - p1; // vektor a
    Tacka vB = p3 - p1; // vektor b
    double sTeta = vA[0] * vB[1] - vB[0] * vA[1]; // sinus teta
    if (sTeta > 0.0) return 1; // pozitivna orjentacija
    else if (sTeta < 0.0) return -1; // negativna orjentacija
    return 0; // povrsina trokuta jednaka je nuli, tj. ta�ke su kolinearne
}




int main()
{


    cout << "Hello world!" << endl;
    return 0;
}
