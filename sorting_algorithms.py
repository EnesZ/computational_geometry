
list1 = [5,4,3,2,1]
n = len(list1)


def swap(list,i,j):
    temp = list[i]
    list[i]= list[j]
    list[j]=temp

def bubble(list1, n):
    for i in range(n):
        for j in range(n-i-1):
            if list1[j] > list1[j+1]:
                swap(list1,j,j+1)

def insertion(list1,n):
    for i in range(0,n-1):
        j=i
        while j >= 0 and list1[j]>list1[j+1]:
            swap(list1,j,j+1)
            j-=1

def selection(list1,n):
    for i in range(n-1):
        min_x = list1[i]
        index = i

        for j in range(i+1,n):
            if list1[j]< min_x:
                min_x = list1[j]
                index = j

        swap(list1,i,index)

def merge(l1,l2):
    i=j=0
    n= len(l1)
    m=len(l2)
    l=[]
    while i<n and j<m:
        if l1[i]<l2[j]:
            l.append(l1[i])
            i+=1
        else:
            l.append(l2[j])
            j+=1

    while i<n:
        l.append(l1[i])
        i +=1
    while j<m:
        l.append(l2[j])
        j +=1

    return l

#print(merge([1,2,3],[2,4]))
def merge_sort(list1,n):
    if n<=1:
        return list1
    else:
        l1 = merge_sort(list1[:int(n/2)],int(n/2))
        l2 = merge_sort(list1[int(n/2):],len(list1[int(n/2):]))
        #print(len(list1[int(n/2):])," ",int(n/2)," ")
        return merge(l1,l2)

def shell(list1,n):
    gap = int(n/2)

    while gap>0:
        for i in range(gap,n):
            temp = list1[i]
            j=i
            while j>=gap and list1[j-gap]>temp:
                list1[j] = list1[j - gap]
                j-=gap
            list1[j]=temp
        gap=int(gap/2)


shell(list1,n)
print(list1)
#print(merge_sort(list1,n))
#insertion(list1,n)
#bubble(list1,n)
#selection(list1,n)


