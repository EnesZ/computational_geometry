from convex_hull_naive import Vertex,Edge, ori

x1=Vertex(0, 0)
x2=Vertex(1, 1)
x3=Vertex(2, 0)
x4=Vertex(0, 2)
x5=Vertex(2, 2)
x6=Vertex(1, 0)
x7=Vertex(3, 0)
x8=Vertex(-1,0)


vertices = (x1,x2,x3,x4,x5,x6,x7,x8)

def graham_scan(vertices):
    left_point = find_left_point(vertices)
    upper_vertices, lower_vertices = divide_vertices(vertices,left_point)
    pseudo_hull_upper = find_hull(upper_vertices)
    pseudo_hull_lower = find_hull(lower_vertices)
    return pseudo_hull_upper+pseudo_hull_lower

def find_left_point(vertices):
    return min(vertices,key= lambda v: v.x)

def divide_vertices(vertices,ref_point):
    upper_vertices = []
    lower_vertices = []
    vertices = sorted(vertices, key=lambda v: v.x)
    for v in vertices:
        if v.y < ref_point.y:
            lower_vertices.append(v)
        else:
            upper_vertices.append(v)

    lower_vertices = sorted(lower_vertices, key=lambda v: -v.x)

    return upper_vertices,lower_vertices

def find_hull(vertices):
    hull = vertices[:2]
    for v in vertices[2:]:
        while ori(Edge(v1=hull[-2],v2=hull[-1]),v)>0:
            hull.pop()
            if len(hull)<2:
                break
        hull.append(v)
    return hull

vr=graham_scan(vertices)
for v in vr:
    print(v)
