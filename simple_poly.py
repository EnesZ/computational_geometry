import numpy as np

x5=(1,2)
x1=(0,0)
x2=(1,1)
x3=(1,0)
x4=(0,1)

vertices = (x1,x2,x3,x4,x5)

"""
Ideja je pronaci referentnu tacku tako sto pronadjemo tacku koja ima najmanju y-koordinatu
(ako ih ima vise uzimamo onu koja ima najvecu x-koordinatu)(moguce je koristiti i neke druge uvjete
za referentnu tacku)

Imaginarnu tacku formiramo tako sto povecamo x-koordinatu ref. tacke za neku vrijednost (u zadatku za 1)

Uzimamo svaku tacku x iz niza tacaka i posmatramo ugao koji formiraju vektori vec(ref_p,im_p) 
i vec(ref_p,x)

Niz tacaka sortiramo po izracunatom uglu. Ako postoji vise tacaka koje imaju isti ugao, niz se 
sortira po udaljenosti date tacke x i referentne tacke

U tako sortiranom poretku tacaka crtamo prost poligon 

"""

def dot(x1,x2):
    assert len(x1)==len(x2)
    s=0
    for i in range(len(x1)):
        s+=x1[i]*x2[i]
    return s

def vector(p1,p2):
    assert len(p1)==len(p2)
    v=[]
    for i in range(len(p1)):
        v.append(p2[i]-p1[i])
    return v

def distance(a,b):
    return np.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

def angle(vertex,ref_p,im_p):
    v1 = vector(ref_p,im_p)
    v2 = vector(ref_p,vertex)
    return (np.arccos(dot(v1,v2)/np.sqrt(dot(v1,v1))/np.sqrt(dot(v2,v2))),distance(ref_p,vertex))

def simple_poly(vertices):
    #if we have more vertices with the equal y-coordinates, we take vertex that have max x-coordinate
    ref_p = min(vertices,key=lambda x: (x[1],-x[0]))
    im_p = list(ref_p)
    #im_p have the same y-coordinate like ref_p but x-coor = x-coord+1
    im_p[0]+=1

    sorted_vertices = sorted(vertices, key= lambda x: angle(x,ref_p,im_p))
    return sorted_vertices

print("ordering of vertices for drawing polygon is",simple_poly(vertices))
